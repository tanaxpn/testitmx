import React from "react";

import ProductComponent from "./product";

const Home: React.FC = () => {
  return (
    <div>
      <div className="max-w-full">
        <img
          src="https://assets.toptal.io/images?url=https%3A%2F%2Fbs-uploads.toptal.io%2Fblackfish-uploads%2Fcomponents%2Fblog_post_page%2Fcontent%2Fcover_image_file%2Fcover_image%2F1140699%2Fregular_1708x683_cover-ultimate-ecommerce-design-guide-c99325c2d82a811bd1157d042955a2dd.png"
          alt="TESTITX Logo"
          className="h-60 w-full"
        />
      </div>
      <hr />
      <ProductComponent />
      <hr className="mb-6" />
    </div>
  );
};

export default Home;
