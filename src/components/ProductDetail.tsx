import React, { useState } from "react";
import { useParams } from "react-router-dom";
import { getAllProducts, Product } from "../services/ProductService";
import ProductModal from "./ProductModal";

const ProductDetail: React.FC = () => {
  const { id } = useParams<{ id: string }>();
  const [product, setProduct] = useState(
    getAllProducts().find((p) => p.id === Number(id))
  );

  const [isModalOpen, setIsModalOpen] = useState(false);

  if (!product) {
    return <div>สินค้าไม่พบ</div>;
  }

  const handleEditClick = () => {
    setIsModalOpen(true);
  };

  const handleModalSave = (formData: Product) => {
    setProduct(formData);
  };

  return (
    <div className="container mx-auto py-8">
      <div className="flex">
        <img
          src={product.image}
          alt={product.name}
          className="w-1/2 h-auto object-cover"
        />
        <div className="w-1/2 ml-4">
          <h1 className="text-3xl font-semibold">{product.name}</h1>
          <p className="text-gray-600">{product.description}</p>
          <p className="text-lg font-bold text-green-500">${product.price}</p>
          <button
            onClick={handleEditClick}
            className="text-white bg-green-400 w-full mt-4 rounded-md p-2 hover:text-white hover:bg-yellow-300 border"
          >
            แก้ไข
          </button>
        </div>
        {isModalOpen && (
          <ProductModal
            product={product}
            onSave={handleModalSave}
            onClose={() => setIsModalOpen(false)}
          />
        )}
      </div>
    </div>
  );
};

export default ProductDetail;
