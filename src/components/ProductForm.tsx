import React, { useState, useEffect } from "react";
import { useParams, useNavigate } from "react-router-dom";
import {
  getProductById,
  addProduct,
  updateProduct,
} from "../services/ProductService";

const ProductForm: React.FC = () => {
  const { id } = useParams<{ id: string }>();
  const navigate = useNavigate();

  const [product, setProduct] = useState({
    id: 0,
    name: "",
    description: "",
    image: "",
    price: 0,
  });

  useEffect(() => {
    if (id) {
      const productById = getProductById(Number(id));
      if (productById) {
        setProduct(productById);
      }
    }
  }, [id]);

  const handleInputChange = (
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    const { name, value } = e.target;
    setProduct({
      ...product,
      [name]: value,
    });
  };

  const handleSubmit = () => {
    if (product.id === 0) {
      addProduct(product);
    } else {
      updateProduct(product);
    }
    navigate("/");
  };

  return (
    <div>
      <h1 className="text-2xl font-semibold">แบบฟอร์มสินค้า</h1>
      <form>
        <div>
          <label htmlFor="name">ชื่อสินค้า</label>
          <input
            type="text"
            id="name"
            name="name"
            value={product.name}
            onChange={handleInputChange}
          />
        </div>
        <div>
          <label htmlFor="description">รายละเอียด</label>
          <textarea
            id="description"
            name="description"
            value={product.description}
            onChange={handleInputChange}
          />
        </div>
        <div>
          <label htmlFor="image">URL รูปภาพ</label>
          <input
            type="text"
            id="image"
            name="image"
            value={product.image}
            onChange={handleInputChange}
          />
        </div>
        <div>
          <label htmlFor="price">ราคา</label>
          <input
            type="number"
            id="price"
            name="price"
            value={product.price}
            onChange={handleInputChange}
          />
        </div>
        <button type="button" onClick={handleSubmit}>
          บันทึก
        </button>
      </form>
    </div>
  );
};

export default ProductForm;
