import React, { useState } from "react";
// import { Link } from "react-router-dom";
import { getAllProducts, Product } from "../services/ProductService";
import ProductModal from "./ProductModal";

const ProductComponent: React.FC = () => {
  const [products, setProducts] = useState(getAllProducts());
  const [isModalOpen, setIsModalOpen] = useState(false);
  const handleDelete = (id: number) => {
    const productDel = products;
    const index = productDel.findIndex((product) => product.id === id);
    if (index !== -1) {
      productDel.splice(index, 1);
    }
    setProducts([...productDel]);
  };
  const handleRouter = (id: number) => {
    window.location.href = `/product/${id}`;
  };

  const handleNewProductClick = () => {
    setIsModalOpen(true);
  };
  /* eslint-disable */
  const handleModalSave = (formData: Product) => {
    setIsModalOpen(false);
  };

  return (
    <div className="container mx-auto py-8">
      <div className="relative mb-6">
        <span className="text-3xl font-semibold text-center mb-4">
          PRODUCTS
        </span>
        <button
          onClick={handleNewProductClick}
          className="border rounded-md text-blue-500 float-right p-2 absolute right-0 hover:text-white hover:bg-blue-300"
        >
          เพิ่มสินค้า
        </button>
      </div>
      {isModalOpen && (
        <ProductModal
          product={{ id: 0, name: "", description: "", image: "", price: 0 }}
          onSave={handleModalSave}
          onClose={() => setIsModalOpen(false)}
        />
      )}
      <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-3 xl:grid-cols-4 gap-4">
        {products.map((product) => (
          <div
            className="bg-white p-4 rounded-lg shadow cursor-pointer"
            key={product.id}
          >
            <div onClick={() => handleRouter(product.id)}>
              <img
                src={product.image}
                alt={product.name}
                className="w-full h-32 object-cover mb-2"
              />
              <h2 className="text-lg font-semibold">{product.name}</h2>
              <p className="text-gray-600">{product.description}</p>
              <p className="text-lg font-bold text-green-500">
                ${product.price}
              </p>
            </div>
            <button onClick={() => handleDelete(product.id)}>ลบ</button>
          </div>
        ))}
      </div>
    </div>
  );
};

export default ProductComponent;
