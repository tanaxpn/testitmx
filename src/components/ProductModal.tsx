import React, { useState, useEffect } from "react";
import { addProduct, updateProduct, Product } from "../services/ProductService";

interface Props {
  product: Product;
  onSave: (Product: Product) => void;
  onClose: () => void;
}

const ProductModal: React.FC<Props> = ({ product, onSave, onClose }) => {
  const [formData, setFormData] = useState(product);

  useEffect(() => {
    setFormData(product);
  }, [product]);

  const handleInputChange = (
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };
  const handleImage = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (e?.target?.files != null) {
      getBase64(e.target.files[0], (callback) => {
        setFormData({
          ...formData,
          image: callback,
        });
      });
    }
  };
  const getBase64 = (file: File, cb: any) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () {
      cb(reader.result);
    };
    reader.onerror = function (error) {
      console.log("Error: ", error);
    };
  };

  const handleSubmit = () => {
    if (formData.id === 0) {
      addProduct(formData);
    } else {
      updateProduct(formData);
    }
    onSave(formData);
    onClose();
  };

  return (
    <div
      className="fixed inset-0 flex items-center justify-center z-50 backdrop-blur-lg"
      style={{ backgroundColor: "rgba(0, 0, 0, 0.4)" }}
    >
      <div className="bg-white w-96 p-6 rounded-lg shadow-lg border">
        <h2 className="text-xl font-semibold mb-4">
          {formData.id === 0 ? "เพิ่มสินค้า" : "แก้ไขสินค้า"}
        </h2>
        <form>
          <div className="mb-4 text-start">
            <label htmlFor="name">ชื่อสินค้า</label>
            <input
              type="text"
              id="name"
              name="name"
              value={formData.name}
              onChange={handleInputChange}
              className="w-full border rounded px-3 py-2"
            />
          </div>
          <div className="mb-4 text-start">
            <label htmlFor="name">Description</label>
            <input
              type="text"
              id="description"
              name="description"
              value={formData.description}
              onChange={handleInputChange}
              className="w-full border rounded px-3 py-2"
            />
          </div>
          <div className="mb-4 text-start">
            <label htmlFor="name">ราคา</label>
            <input
              type="number"
              id="price"
              name="price"
              value={formData.price}
              onChange={handleInputChange}
              className="w-full border rounded px-3 py-2"
            />
          </div>
          <div className="mb-4 text-start">
            <label htmlFor="name">Image</label>
            <input
              type="file"
              id="image"
              name="image"
              onChange={handleImage}
              className="w-full border rounded px-3 py-2"
            />
          </div>
          <div className="flex justify-end">
            <button
              type="button"
              onClick={onClose}
              className="mr-2 px-4 py-2 border rounded hover:bg-gray-200"
            >
              ยกเลิก
            </button>
            <button
              type="button"
              onClick={handleSubmit}
              className="px-4 py-2 bg-blue-500 text-white rounded hover:bg-blue-600"
            >
              บันทึก
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default ProductModal;
