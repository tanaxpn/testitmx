import productsData from "../product.json";

export interface Product {
  id: number;
  name: string;
  description: string;
  image: string;
  price: number;
}

export const getAllProducts = (): Product[] => {
  return productsData;
};

export const getProductById = (id: number): Product | undefined => {
  return productsData.find((product) => product.id === id);
};

export const addProduct = (product: Product): void => {
  productsData.push({
    id: productsData.length + 1,
    name: product.name,
    description: product.description,
    price: product.price,
    image: product.image,
  });
};

export const updateProduct = (product: Product): void => {
  const index = productsData.findIndex((p) => p.id === product.id);
  if (index !== -1) {
    productsData[index] = product;
  }
};
